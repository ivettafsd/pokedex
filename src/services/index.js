import axios from 'axios';

export const baseUrl = 'https://pokeapi.co/api/v2';
axios.defaults.baseURL = baseUrl;

export const getPokemons = async ({ offset, limit }) => {
  try {
    const { data } = await axios.get(
      `/pokemon/?limit=${limit}&offset=${offset}`,
    );
    return data.results;
  } catch (error) {
    console.log('error', error);
  }
};

export const getPokemonById = async ({ id }) => {
  try {
    const { data } = await axios.get(`/pokemon/${id}`);
    return data;
  } catch (error) {
    console.log('error', error);
  }
};

export const getPokemonsByType = async ({ id }) => {
  try {
    const { data } = await axios.get(`/type/${id}`);
    return data.pokemon.map(({ pokemon }) => pokemon);
  } catch (error) {
    console.log('error', error);
  }
};

export const getTypes = async () => {
  try {
    const { data } = await axios.get(
      'https://pokeapi.co/api/v2/type?limit=999',
    );
    return data.results;
  } catch (error) {
    console.log('error', error);
  }
};
