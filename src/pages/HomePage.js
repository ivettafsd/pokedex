import { useState, useRef } from 'react';
import { useAsync } from 'react-use';
import { useParams } from 'react-router-dom';
import { Title } from '../components/Title';
import { PokemonList } from '../components/PokemonList';
import { PokemonDetails } from '../components/PokemonDetails';
import { Filters } from '../components/Filters';
import { Loader } from '../components/Loader';
import { ModalPokemon } from '../components/ModalPokemon';
import { useMediaQuery } from '../hooks/useMediaQuery';
import {
  getPokemons,
  getTypes,
  getPokemonById,
  getPokemonsByType,
  baseUrl,
} from '../services';
import { getRandomColor } from '../helpers';
import styles from './HomePage.module.css';

const HomePage = () => {
  const { pokemonId } = useParams();
  const btnRef = useRef();
  const matches = useMediaQuery('(min-width: 768px)');

  const [page, setPage] = useState(1);
  const [pokemons, setPokemons] = useState([]);
  const [openedPokemon, setOpenedPokemon] = useState(null);
  const [types, setTypes] = useState([]);
  const [selectedType, setSelectedType] = useState();
  const [showLoader, setShowLoader] = useState(false);

  const scrollToRef = () => {
    btnRef.current &&
      btnRef.current.scrollIntoView({ behavior: 'smooth', block: 'start' });
  };

  const fetchFullDataPokemon = async (pokemons) => {
    setShowLoader(true);
    const fullDataPokemons = await Promise.allSettled(
      pokemons.map((pokemon) => {
        const pokemonId = pokemon.url
          .replace(`${baseUrl}/pokemon/`, '')
          .replace('/', '');

        return getPokemonById({ id: pokemonId });
      }),
    );
    const updatedPokemons = fullDataPokemons.map((data) =>
      data.status ? data.value : null,
    );
    setPokemons((prevPokemons) => {
      if (selectedType) {
        return updatedPokemons;
      } else {
        return [...prevPokemons, ...updatedPokemons];
      }
    });
    setShowLoader(false);
    if (page > 1)
      setTimeout(() => {
        scrollToRef();
      }, 100);
  };
  const fetchPokemons = async () => {
    const limit = 12;
    const offset = (page - 1) * limit;
    const newPokemons = await getPokemons({ offset, limit });
    await fetchFullDataPokemon(newPokemons);
  };

  useAsync(async () => {
    const currentTypes = await getTypes();
    const updatedCurrentTypes = currentTypes.map((type) => {
      return { ...type, color: getRandomColor() };
    });
    setTypes(updatedCurrentTypes);
  }, []);

  useAsync(async () => {
    if (pokemonId) {
      const foundPokemon = pokemons?.find(
        (pokemon) => pokemon.id === pokemonId,
      );
      if (foundPokemon) {
        setOpenedPokemon(foundPokemon);
      } else {
        const pokemon = await getPokemonById({ id: pokemonId });
        setOpenedPokemon(pokemon);
      }
    } else {
      setOpenedPokemon(null);
    }
  }, [pokemonId, pokemons]);

  useAsync(async () => {
    setPokemons([]);
    if (selectedType) {
      setPage(0);
      const pokemons = await getPokemonsByType({ id: selectedType });
      await fetchFullDataPokemon(pokemons);
    } else {
      setPage(1);
    }
  }, [selectedType]);

  useAsync(async () => {
    page && (await fetchPokemons());
  }, [page]);

  const handleClickLoadMore = () => {
    setPage((prev) => prev + 1);
  };

  return (
    <main className={styles.main}>
      <Title />
      <div className={styles.page}>
        <section className={styles.list}>
          {types && (
            <Filters
              types={types}
              currentSelectedType={selectedType}
              onSelect={setSelectedType}
            />
          )}
          {pokemons.length > 0 && types.length > 0 && (
            <PokemonList
              pokemons={pokemons}
              types={types}
              selectedType={selectedType}
              onSelectType={setSelectedType}
            />
          )}
          {selectedType && showLoader && <Loader />}
          {!selectedType && (
            <>
              {showLoader ? (
                <Loader />
              ) : (
                <button
                  ref={btnRef}
                  className={styles.btn}
                  type="button"
                  onClick={handleClickLoadMore}
                >
                  Load more
                </button>
              )}
            </>
          )}
        </section>
        <section className={styles.details}>
          {pokemonId && openedPokemon && (
            <>
              {!matches ? (
                <ModalPokemon>
                  <PokemonDetails pokemon={openedPokemon} />
                </ModalPokemon>
              ) : (
                <PokemonDetails pokemon={openedPokemon} matches={matches} />
              )}
            </>
          )}
        </section>
      </div>
    </main>
  );
};

export default HomePage;
