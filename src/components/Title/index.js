import styles from './Title.module.css';

export const Title = () => {
  return <h1 className={styles.title}>Pokedex</h1>;
};
