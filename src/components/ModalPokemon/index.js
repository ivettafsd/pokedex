import Modal from 'react-modal';

Modal.setAppElement('#root');

export const ModalPokemon = ({ children }) => {
  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      backgroundColor: '#000',
      width: '100vw',
      height: '100vh',
    },
  };
  return (
    <Modal isOpen={true} style={customStyles}>
      {children}
    </Modal>
  );
};
