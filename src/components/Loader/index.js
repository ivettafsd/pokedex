import pokeball from '../../assets/pokeball.png';
import styles from './Loader.module.css';

export const Loader = () => (
  <div className={styles.loader}>
    <img
      src={pokeball}
      className={styles.loaderIcon}
      width="60"
      height="60"
      alt="Loader"
    />
  </div>
);
