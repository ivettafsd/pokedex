import { Link } from 'react-router-dom';
import { nanoid } from 'nanoid';
import defaultImage from '../../assets/pokeball.png';
import styles from './PokemonItem.module.css';

export const PokemonItem = ({ pokemon, types, onSelectType }) => {
  const handleClickType = (e, item) => {
    e.preventDefault();
    onSelectType(item.type.name);
  };
  return (
    <li>
      <Link className={styles.preview} to={`/${pokemon.id}`} state={pokemon}>
        <img
          className={styles.img}
          src={
            pokemon.sprites.other['official-artwork']?.front_default
              ? pokemon.sprites.other['official-artwork']?.front_default
              : defaultImage
          }
          width={65}
          height={65}
          alt={pokemon.name}
        />
        <div>
          <h2 className={styles.name}>
            {pokemon.name.length > 17
              ? pokemon.name.slice(0, 17) + '...'
              : pokemon.name}
          </h2>
          <ul className={styles.types}>
            {pokemon.types.map((item) => {
              const backgroundColor = types.find(
                (type) => type.name === item.type.name,
              ).color;

              return (
                <li
                  className={styles.type}
                  key={nanoid()}
                  style={{ backgroundColor }}
                  onClick={(e) => handleClickType(e, item)}
                >
                  {item.type.name}
                </li>
              );
            })}
          </ul>
        </div>
      </Link>
    </li>
  );
};
