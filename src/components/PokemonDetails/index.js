import { QRCode } from 'react-qr-svg';
import { Link } from 'react-router-dom';
import { nanoid } from 'nanoid';
import { characters } from '../../constants';
import styles from './PokemonDetails.module.css';

export const PokemonDetails = ({ pokemon, matches }) => {
  return (
    <div className={styles.card}>
      {!matches && (
        <Link className={styles.back} to={'/'}>
          Back
        </Link>
      )}
      <div className={styles.imgBox}>
        <img
          alt={pokemon.name}
          className={styles.img}
          src={pokemon.sprites.other['official-artwork']?.front_default}
        />
        <QRCode
          level="M"
          className={styles.qr}
          value={`https://ivetta-pokedex.netlify.app/${pokemon.id}`}
        />
      </div>
      <h3 className={styles.name}>{`${pokemon.name} #${pokemon.id
        .toString()
        .padStart(3, '0')}`}</h3>
      <table className={styles.table}>
        <tbody>
          <tr>
            <td>{characters.type}</td>
            <td>
              {pokemon.types.map(({ type }) => (
                <span className={styles.type} key={nanoid()}>
                  {type.name}
                </span>
              ))}
            </td>
          </tr>
          {pokemon.stats.map(({ stat, base_stat }) => (
            <tr key={nanoid()}>
              <td>{characters[stat.name]}</td>
              <td>{base_stat}</td>
            </tr>
          ))}
          <tr>
            <td>{characters.weight}</td>
            <td>{pokemon.weight}</td>
          </tr>
          <tr>
            <td>{characters['total-moves']}</td>
            <td>{pokemon.moves.length}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};
