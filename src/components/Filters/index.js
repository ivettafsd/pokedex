import Select from 'react-select';
import { baseUrl } from '../../services';
import styles from './Filters.module.css';

export const Filters = ({ types, currentSelectedType, onSelect }) => {
  const updatedTypes = [
    ...types.map((type) => ({
      value: type.url.replace(`${baseUrl}/type/`, '').replace('/', ''),
      label: type.name,
      color: type.color,
    })),
  ];
  const selectedType = updatedTypes.find(
    (type) => type.label === currentSelectedType,
  );

  const customStyles = {
    control: (provided, state) => ({
      ...provided,
      border: '1px solid #fff',
      borderRadius: '15px',
      width: '280px',
      backgroundColor: 'transparent',
      marginBottom: '15px',
      color: '#fff',
      boxShadow: state.isFocused ? '0 0 0 3px #000' : null,
      '&:hover': {
        borderColor: ' #000',
        outline: 'none',
      },
      '&:focus': {
        borderColor: ' #000',
        outline: 'none',
      },
    }),
    option: (provided, state) => {
      return {
        ...provided,
        backgroundColor: state.data.color,
        display: 'inline-block',
        width: 'auto',
        margin: '2px',
        borderRadius: '5px',
        color: '#000',
        '&:hover': {
          backgroundColor: 'lightblue',
        },
      };
    },
    menu: (provided) => ({
      ...provided,
      width: '280px',
      backgroundColor: '#fff',
      overflow: 'hidden',
      borderRadius: '15px',
      boxShadow: '0px 2px 10px rgba(0, 0, 0, 0.1)',
      marginTop: '4px',
      zIndex: '2',
      display: 'flex',
      justifyContent: 'center',
      flexWrap: 'wrap',
      textTransform: 'capitalize',
    }),
    placeholder: (provided) => ({
      ...provided,
      color: '#fff',
    }),
    valueContainer: (provided, state) => {
      return {
        ...provided,
        background: state.getValue()[0]
          ? state.getValue()[0].color
          : 'transparent',
        color: '#000',
        padding: '1px',
        marginLeft: '6px',
        borderRadius: '5px',
        width: 'auto',
        textTransform: 'capitalize',
      };
    },
  };

  return (
    <div className={styles.filters}>
      <Select
        className="basic-single"
        classNamePrefix="select"
        isClearable={true}
        name="type"
        placeholder="Select..."
        value={selectedType}
        options={updatedTypes}
        styles={customStyles}
        onChange={(e) => (e ? onSelect(e.label) : onSelect(null))}
      />
    </div>
  );
};
