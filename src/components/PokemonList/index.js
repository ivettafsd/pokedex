import { nanoid } from 'nanoid';
import { PokemonItem } from '../PokemonItem';

import styles from './PokemonList.module.css';

export const PokemonList = ({
  pokemons = [],
  types = [],
  selectedType,
  onSelectType,
}) => {
  return (
    <ul className={`${styles.list} ${selectedType && styles.selectedType}`}>
      {pokemons.length > 0 &&
        pokemons.map((pokemon) => (
          <PokemonItem
            key={nanoid()}
            pokemon={pokemon}
            types={types}
            onSelectType={onSelectType}
          />
        ))}
    </ul>
  );
};
