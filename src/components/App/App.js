import { Suspense, lazy } from 'react';
import { Routes, Route } from 'react-router-dom';
import { Loader } from '../Loader';

const HomePage = lazy(() => import('../../pages/HomePage'));

function App() {
  return (
    <Suspense fallback={<Loader />}>
      <Routes>
        <Route index element={<HomePage />} />
        <Route path="/:pokemonId" element={<HomePage />} />
      </Routes>
    </Suspense>
  );
}

export default App;
