export const characters = {
  type: 'Type',
  attack: 'Attack',
  defense: 'Defense',
  hp: 'HP',
  'special-attack': 'SP Attack',
  'special-defense': 'SP Defense',
  speed: 'Speed',
  weight: 'Weight',
  'total-moves': 'Total moves',
};
